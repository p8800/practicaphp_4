<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $Codigo
 * @property string $Nombre
 * @property string $Apellido1
 * @property string $Apellido2
 * @property string $Direccion
 * @property string $Poblacion
 * @property string $FechaNacimiento
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Apellido1', 'Apellido2', 'Direccion', 'Poblacion', 'FechaNacimiento'], 'required'],
            [['FechaNacimiento'], 'safe'],
            [['Nombre', 'Apellido1', 'Apellido2', 'Poblacion'], 'string', 'max' => 100],
            [['Direccion'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Codigo' => 'Codigo',
            'Nombre' => 'Nombre',
            'Apellido1' => 'Apellido 1',
            'Apellido2' => 'Apellido 2',
            'Direccion' => 'Direccion',
            'Poblacion' => 'Poblacion',
            'FechaNacimiento' => 'Fecha Nacimiento',
        ];
    }
}
